<?php

class TemplateManager
{
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }
    
    /**
     * @param  string $text
     * @param array $data
     *
     * @return mixed|string
     */
    private function computeText($text, array $data)
    {
        $quote = $this->getQuote($data);

        if ($quote)
        {
            $quoteFromRepository = QuoteRepository::getInstance()->getById($quote->id);
            $destinationOfQuote = DestinationRepository::getInstance()->getById($quote->destinationId);
            
            $text = $this->computeQuoteDestination($destinationOfQuote,$text,$quoteFromRepository);
            $text = str_replace(array('[quote:summary_html]','[quote:summary]'),array( Quote::renderHtml($quoteFromRepository),Quote::renderText($quoteFromRepository)), $text);
           
        }
        
        $user = $this->getUser($data);
        /*
         * USER
         * [user:*]
         */
        if($user) {
            $text = str_replace('[user:first_name]'       , ucfirst(mb_strtolower($user->firstname)), $text);
        }

        return $text;
    }
    
    /**
     * Get quote from data array
     * @param array $data
     *
     * @return null|\Quote
     */
    private function getQuote(array $data) : ?Quote{
        if(isset($data['quote']) && $data['quote'] instanceof Quote){
            return $data['quote'];
        }
        return null;
    }
    
    /**
     * Get user from array
     * @param array $data
     *
     * @return null|\User
     */
    private function getUser(array $data) : ?User{
        if(isset($data['user']) &&  $data['user']  instanceof User){
            return $data['user'];
        }else{
            $APPLICATION_CONTEXT = ApplicationContext::getInstance();
            return $APPLICATION_CONTEXT->getCurrentUser();
        }
    }
    /**
     * Compute quote destination
     * @param \Destination|null $destination
     * @param String            $text
     * @param \Quote            $quote
     *
     * @return string
     */
    private function computeQuoteDestination(Destination $destination = null, string $text, Quote $quote) :string {
        if($destination){
            $siteFromRepository = SiteRepository::getInstance()->getById($quote->siteId);
            $text = str_replace(array('[quote:destination_link]','[quote:destination_name]'),array($siteFromRepository->url . '/' . $destination->countryName . '/quote/' . $quote->id,$destination->countryName) , $text);
        }else{
            $text = str_replace('[quote:destination_link]', '', $text);
        }
        return $text;
    }
}
